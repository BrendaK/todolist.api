<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserTypeController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\TaskController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('users', [UserController::class, 'getAll']);
Route::get('users/{id}', [UserController::class, 'getById']);
Route::post('users', [UserController::class, 'create']);
Route::post('users/{id}', [UserController::class, 'update']);
Route::delete('users/{id}', [UserController::class, 'delete']);

Route::get('usersTypes', [UserTypeController::class, 'getAll']);
Route::get('usersTypes/{id}', [UserTypeController::class, 'getById']);
Route::post('usersTypes', [UserTypeController::class, 'create']);
Route::post('usersTypes/{id}', [UserTypeController::class, 'update']);
Route::delete('usersTypes/{id}', [UserTypeController::class, 'delete']);

Route::get('categories', [CategoryController::class, 'getAll']);
Route::get('categories/{id}', [CategoryController::class, 'getById']);
Route::post('categories', [CategoryController::class, 'create']);
Route::post('categories/{id}', [CategoryController::class, 'update']);
Route::delete('categories/{id}', [CategoryController::class, 'delete']);

Route::get('tasks', [TaskController::class, 'getAll']);
Route::get('tasks/user/{userId}', [TaskController::class, 'getById']);
Route::post('tasks', [TaskController::class, 'create']);
Route::post('tasks/{id}', [TaskController::class, 'update']);
Route::delete('tasks/{id}', [TaskController::class, 'delete']);