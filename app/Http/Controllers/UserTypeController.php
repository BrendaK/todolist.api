<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use\App\Models\UserType;

class UserTypeController extends Controller
{
    public function getAll(){
        $userTypes =UserType::all();
        return Response::json($userTypes, 200);
    }

    public function getById($id){
        $userType =UserType::find($id);
        return Response::json($userType, 200);
    }

    public function create(Request $request){

        $userType =new UserType;

        $userType = UserType::new($userType, $request);

        return Response::json($userType, 200);
  
    }

    public function update($id, Request $request){
        $userType =UserType::find($id);
        
        $userType = UserType::new($userType, $request);

        return Response::json($userType, 200);

    }

    public function delete($id){
        UserType::destroy($id);
        return Response::json("record deleted", 200);
    }
}
