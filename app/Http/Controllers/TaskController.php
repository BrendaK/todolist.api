<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function getAll(){
        $tasks = Task::with(["categories", "user"])->paginate(10);
        return Response::json($tasks, 200);
    }

    public function getByUser($userId){
        $task = Task::with(["categories", "user"])
        ->where("user_id", $userId)->paginate(10);

        return Response::json($task, 200);
    }

    public function create(Request $request){
        if(!$request->confirm_password) return Response::json("confirm_password missing", 500);

        $task =new Task;

        $task = Task::new($task, $request);

        return Response::json($task, 200);
  
    }

    public function update($id, Request $request){
        $task = Task::find($id);
        
        $task = Task::new($task, $request);

        return Response::json($task, 200);

    }

    public function delete($id){
        Task::destroy($id);
        return Response::json("record deleted", 200);
    }
}
