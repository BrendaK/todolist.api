<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use\App\Models\Category;


class CategoryController extends Controller
{
    public function getAll(){
        $categories = Category::all();
        return Response::json($categories, 200);
    }

    public function getById($id){
        $category = Category::find($id);
        return Response::json($category, 200);
    }

    public function create(Request $request){
        if(!$request->confirm_password) return Response::json("confirm_password missing", 500);

        $category =new Category;

        $category = Category::new($category, $request);

        return Response::json($category, 200);
  
    }

    public function update($id, Request $request){
        $category = Category::find($id);
        
        $category = Category::new($category, $request);

        return Response::json($category, 200);

    }

    public function delete($id){
        Category::destroy($id);
        return Response::json("record deleted", 200);
    }
}
