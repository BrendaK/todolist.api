<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Category;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                "lastname" => "Liqueur",
                "firstname" => "Lili",
                "email" => "lililiqueur@gmail.com",
                "user_type_id" => 1,
                "password" => "secret"
            ],
            [
                "lastname" => "lala",
                "firstname" => "lola",
                "email" => "lolalala@gmail.com",
                "user_type_id" => 2,
                "password" => "secret"

            ]
        ];

        $categories = [
            [
                "name"=>"primary", "color"=>"primary",
            ],
            [
                "name"=>"secondary", "color"=>"secondary",
            ],
            [
                "name"=>"success", "color"=>"success",
            ],
            [
                "name"=>"info", "color"=>"info",
            ],
            [
                "name"=>"warning", "color"=>"warning",
            ],
            [
                "name"=>"danger", "color"=>"danger",
            ],
            [
                "name"=>"light", "color"=>"light",
            ],
            [
                "name"=>"dark", "color"=>"dark",
            ]
        ];
        
        foreach($users AS $user):
            $user = User::create($user);
        
            foreach($categories AS $category):
                $category['user_id'] = $user->id;
                Category::create($category);
            endforeach;
        endforeach;
    }
}


