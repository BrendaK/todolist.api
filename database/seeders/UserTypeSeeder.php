<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\UserType;


class UserTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userTypes = [
            [
                "name" => "administrateur",   
            ],
            [
                "name" => "utilisateur",   
            ]
        ];

        foreach($userTypes AS $userType):
            UserType::create($userType);
        endforeach;
    }
}
