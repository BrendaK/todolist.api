<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Task;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories =[
            [1,2,3,4,5,6,7,8],
            [9,10,11,12,13,14,15,16],
        ];

        for($i = 1 ; $i < 1001; $i++):
            $userId = rand(1,2);
           $task = Task::create([
                "name" => "Task" .$i,
                "due_to" => "2021-02-01 08:00:00",
                "user_id" => $userId
            ]);

        $task->categories()->attach([
            $categories[$userId-1][rand(0,2)],
            $categories[$userId-1][rand(3,5)],
            ]);
        endfor;
    }
}
